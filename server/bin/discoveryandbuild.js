var loopback = require('loopback');
var ds = loopback.createDataSource('postgresql', {"host": "10.110.161.63",
    "port": 5432,
    "url": "",
    "database": "hybriskyma",
    "password": "password",
    "name": "postgres",
    "user": "postgres",
    "connector": "postgresql"});

// Discover and build models from INVENTORY table
ds.discoverAndBuildModels('Hybris', {visited: {}, associations: true},
function (err, models) {
  // Now we have a list of models keyed by the model name
  // Find the first record from the inventory
  models.Hybris.findOne({}, function (err, inv) {
    if(err) {
      console.error(err);
      return;
    }
    console.log("\Hybris: ", inv);
    // Navigate to the product model
    // Assumes inventory table has a foreign key relationship to product table
    inv.product(function (err, prod) {
      console.log("\nProduct: ", prod);
      console.log("\n ------------- ");
    });
  });
});
